<div id="CreateGalleryContent" style="padding-left:5px;padding-top:5px;overflow:auto;height:100%;display:none;" >
    
    <div style="width:100%;height:auto;padding:15px;">

        <h3 style="padding-left:5%;font-weight:500;font-size:24;">Follow each step to create a new gallery</h3>
        <div class="line-spacer" style="height:2px;background-color: #0090c5;width:90%;margin:5%;margin-top:0px;margin-bottom:10px;"></div>
        <p style="padding-left:5%;padding-right:5%;font-weight:400;font-size:18;">First you need to give your new gallery a name and description then decide how big you want your new gallery to be and choose the interior decor. Then you will be able to upload images and share with the world.</p>

    </div>

    <!-- Content Here !-->

    <div id="createGallery-Step1">

    
    <!-- Gallery Details !-->
    <h3 style="padding-left:5%;font-weight:500;font-size:20;">Gallery Details</h3>
    <div class="line-spacer" style="height:1px;background-color: #0090c5;width:90%;margin:5%;margin-top:0px;margin-bottom:10px;"></div>

    <div style="width:90%;height:200px;margin:5%;margin-top:0px;margin-bottom:0px;border-radius:5px;background-color:rgba(194, 194, 194, 0.4);">

    <form>

    <div style="width:100%;height:100%;">
    <div style="width:50%;float:left;padding:10px;">
    <p style="padding-left:5%;padding-right:5%;padding-top:0px;padding-top:0px;font-weight:400;font-size:20;">Gallery Name</p>       
    <input name="name" type="text" required>
    </div>

    <div style="width:50%;height:100%;float:right;padding:10px;overflow:auto;">
    <p style="padding-left:5%;padding-right:5%;padding-top:0px;padding-bottom:0px;font-weight:400;font-size:20;">Gallery Description</p>            
    <textarea  name="description" type="text" cols="40" rows="2" required> </textarea>
    </div>


    </div>

    </form>


    </div>
    <!-- Gallery Details END!-->

        <div style="width:100%;height:auto;padding-top:30px;margin-bottom:50px;">

            <h3 style="padding-left:5%;font-weight:500;font-size:20;">Choose a gallery size</h3>
            <div class="line-spacer"></div>

            <div style="width:90%;height:250px;margin:5%;margin-top:0px;margin-bottom:0px;border-radius:5px;background-color:rgba(194, 194, 194, 0.4);">

                <div style="width:33%;height:250px;float:left;">
                    <div class="content-card"  style="width:90%;height:85%;">
                    <h1>Small (10 Images)</h1>            
                    <div class="line-spacer"></div>
                    <img src="images/Small_Blueprint.png" width="211" height="91">
                    <a href="" style="display:none;">
                    <div class="button-left">Choose</div>
                    <div class="button-right">BASIC</div>          
                    </a>
                    <div style="display:block;width:90%;height:40px;margin:5%;color:white;background-color: #4ccf00;border-radius:5px;text-align:center;">Active</div>

                    </div>
                </div>

                <div style="width:33%;height:250px;float:left;">
                    <div  class="content-card" style="width:90%;height:85%;">
                    <h1 >Medium (20 Images)</h1>            
                    <div class="line-spacer" ></div>
                    <img src="images/Medium_Blueprint.png" width="211" height="91">

                    <a href="" style="display:block;">
                    <div class="button-left">Choose</div>
                    <div class="button-right">PLUS</div>          
                    </a>
                    <div style="display:none;width:90%;height:40px;margin:5%;color:white;background-color: #4ccf00;border-radius:5px;text-align:center;">Active</div>

                    </div>
                </div>

                <div style="width:33%;height:250px;float:left;">
                    <div  class="content-card" style="width:90%;height:85%;">
                    <h1>Large (35 Images)</h1>            
                    <div class="line-spacer"></div>
                    <img src="images/Large_Blueprint.png" width="211" height="91">

                    
                    <a href="" style="display:block;">
                    <div class="button-left">Choose</div>
                    <div class="button-right">PRO</div>          
                    </a>
                    <div style="display:none;width:90%;height:40px;margin:5%;color:white;background-color: #4ccf00;border-radius:5px;text-align:center;">Active</div>

                    </div>
                </div>




            </div>
    <br>
                <h3 style="padding-left:5%;font-weight:500;font-size:20;">Choose a style</h3>
                <div class="line-spacer"></div>


                <div style="width:90%;height:260px;margin:5%;margin-top:0px;margin-bottom:0px;border-radius:5px;background-color:rgba(194, 194, 194, 0.4);">

    <div>

<div style="width:33%;height:120px;float:left;">
    <div  class="content-card" style="width:90%;height:85%;">
    <h1>Modern</h1>            
    <div class="line-spacer"></div>


    <a href="" style="display:none;">
    <div style="float:left;width:50%;height:40px;margin:5%;margin-right:0px;margin-top:0px;color:white;background-color: #0090c5;border-radius:5px;border-top-right-radius:0px;border-bottom-right-radius:0px;text-align:center;">Choose</div>
    <div style="float:left;width:40%;height:40px;margin:5%;margin-left:0px;color:white;margin-top:0px;background-color: #55555555;border-radius:5px;border-top-left-radius:0px;border-bottom-left-radius:0px;text-align:center;">BASIC</div>          
    </a>
    <div style="display:block;width:90%;height:40px;margin:5%;color:white;background-color: #4ccf00;border-radius:5px;text-align:center;margin-top:0px;border-top-left-radius:0px;border-top-right-radius:0px;">Active</div>

    </div>
</div>

<div style="width:33%;height:120px;float:left;">
    <div  class="content-card" style="width:90%;height:85%;">
    <h1>Industrial</h1>            
    <div class="line-spacer"></div>


    <a href="" style="display:block;">
    <div style="float:left;width:50%;height:40px;margin:5%;margin-right:0px;color:white;margin-top:0px;background-color: #0090c5;border-radius:5px;border-top-right-radius:0px;border-bottom-right-radius:0px;text-align:center;border-top-left-radius:0px;border-top-right-radius:0px;">Choose</div>
    <div style="float:left;width:40%;height:40px;margin:5%;margin-left:0px;color:white;margin-top:0px;background-color: #55555555;border-radius:5px;border-top-left-radius:0px;border-bottom-left-radius:0px;text-align:center;border-top-left-radius:0px;border-top-right-radius:0px;">PLUS</div>          
    </a>
    <div style="display:none;width:90%;height:40px;margin:5%;color:white;background-color: #4ccf00;border-radius:5px;text-align:center;margin-top:0px;border-top-left-radius:0px;border-top-right-radius:0px;">Active</div>

    </div>
</div>

<div style="width:33%;height:120px;float:left;">
    <div  class="content-card" style="width:90%;height:85%;">
    <h1>Contemporary</h1>            
    <div class="line-spacer"></div>

    
    <a href="" style="display:block;">
    <div style="float:left;width:50%;height:40px;margin:5%;margin-right:0px;color:white;background-color: #0090c5;margin-top:0px;border-radius:5px;border-top-right-radius:0px;border-bottom-right-radius:0px;text-align:center;border-top-left-radius:0px;border-top-right-radius:0px;">Choose</div>
    <div style="float:left;width:40%;height:40px;margin:5%;margin-left:0px;color:white;background-color: #55555555;margin-top:0px;border-radius:5px;border-top-left-radius:0px;border-bottom-left-radius:0px;text-align:center;border-top-left-radius:0px;border-top-right-radius:0px;">PLUS</div>          
    </a>
    <div style="display:none;width:90%;height:40px;margin:5%;color:white;background-color: #4ccf00;border-radius:5px;text-align:center;margin-top:0px;border-top-left-radius:0px;border-top-right-radius:0px;">Active</div>

    </div>
</div>
</div>



<div style="width:33%;height:120px;float:left;">
    <div  class="content-card" style="width:90%;height:85%;">
    <h1>Bohemian</h1>            
    <div class="line-spacer"></div>

    <a href="" style="display:block;">
    <div style="float:left;width:50%;height:40px;margin:5%;margin-right:0px;color:white;background-color: #0090c5;border-radius:5px;margin-top:0px;border-top-right-radius:0px;border-bottom-right-radius:0px;text-align:center;border-top-left-radius:0px;border-top-right-radius:0px;">Choose</div>
    <div style="float:left;width:40%;height:40px;margin:5%;margin-left:0px;color:white;background-color: #55555555;border-radius:5px;margin-top:0px;border-top-left-radius:0px;border-bottom-left-radius:0px;text-align:center;border-top-left-radius:0px;border-top-right-radius:0px;">PRO</div>          
    </a>
    <div style="display:none;width:90%;height:40px;margin:5%;color:white;background-color: #4ccf00;border-radius:5px;text-align:center;margin-top:0px;border-top-left-radius:0px;border-top-right-radius:0px;">Active</div>

    </div>
</div>

<div style="width:33%;height:120px;float:left;">
    <div class="content-card" style="width:90%;height:85%;">
    <h1>Shabby</h1>            
    <div class="line-spacer"></div>
    <a href="" style="display:block;">
    <div style="float:left;width:50%;height:40px;margin:5%;margin-right:0px;color:white;background-color: #0090c5;border-radius:5px;margin-top:0px;border-top-right-radius:0px;border-bottom-right-radius:0px;text-align:center;border-top-left-radius:0px;border-top-right-radius:0px;">Choose</div>
    <div style="float:left;width:40%;height:40px;margin:5%;margin-left:0px;color:white;background-color: #55555555;border-radius:5px;margin-top:0px;border-top-left-radius:0px;border-bottom-left-radius:0px;text-align:center;border-top-left-radius:0px;border-top-right-radius:0px;">PRO</div>          
    </a>
    <div style="display:none;width:90%;height:40px;margin:5%;color:white;background-color: #4ccf00;border-radius:5px;text-align:center;margin-top:0px;border-top-left-radius:0px;border-top-right-radius:0px;">Active</div>

    </div>
</div>

<div style="width:33%;height:120px;float:left;">
    <div style="width:90%;height:85%;background-color:rgba(240, 240, 240, 0.7);border-radius:5px;margin:5%;margin-top:5%;margin-bottom:0px;box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.1);">
    <h1>More Coming Soon!</h1>            
    <div class="line-spacer" style="height:1px;background-color: #0090c5;width:90%;margin:5%;margin-top:0px;margin-bottom:10px;"></div>

    </div>
</div>



        </div>

    </div>



<br>


<!-- Gallery Images !-->
<h3 style="padding-left:5%;font-weight:500;font-size:20;">Gallery Images</h3>
<div class="line-spacer" style="height:1px;background-color: #0090c5;width:90%;margin:5%;margin-top:0px;margin-bottom:10px;"></div>

<div style="width:90%;height:250px;margin:5%;margin-top:0px;margin-bottom:0px;border-radius:5px;background-color:rgba(194, 194, 194, 0.4);">
<p style="padding-left:5%;padding-right:5%;padding-top:0%;font-weight:400;font-size:20;">Select the images you want to use for your new gallery.</p>       

<form>

<div style="width:100%;height:100%;">



</div>

</form>


</div>
<!-- Gallery Images END!-->


    </div>


    <div id="createGallery-Step2">

        <?php

    include "database.php";
    //Validate post data

    $sql = "SELECT * FROM images WHERE owner='$_SESSION[uid]'";
    $result = $conn->query($sql);

    if($result->num_rows > 0){
        // output data of each row
        while($row = $result->fetch_assoc()) {

            echo "
            <div class=\"image-container\" style=\"width:25%;height:200px;float:left;\">
            
                    <div style=\"width:95%;height:95%;border-radius:5px;margin:2.5%;background-color:#ffffff70;box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.1);\">

                                    <div style=\"width:90%;height:90%;text-align:center;border-radius:5px;margin:5%;\"> 
                                    
                                    <img style=\"border-radius:5px;margin:2%;\" width=\"auto\" height=\"96%\" src=\"" ."http://exhibitionshowcase.co.uk/users/" . $_SESSION['uid']. "/images/". $row['id'] . ".jpg" . "\" alt=\"\"/>
                                    
                                    </div> 

                                    
                                    <div style=\"width:100%;text-align:center;margin-top:-140px;\"> 
                            
                                    <div class=\"image-option-overlay\">

                                    <img style=\"border-radius:5px;margin-bottom:-140px;background-color:#0090c5;\" width=\"100%\" height=\"155px;\" src=\" \" alt=\"\"/>
                                    
                                    
                            <form action=\"updatepassword.php\" method=\"post\">

                            <div style=\"width:80%;margin-left:10%;\">
                                <fieldset id=\"email_inputs\"style=\"width:100%;\">
                                    <input class=\"image-option-overlay-input\" id=\"name\" style=\"height:35px;margin-bottom:10px;display:inline;\" type=\"text\" name=\"ImageName\" placeholder=\"". $row['name'] ."\" required>
                                    <input class=\"image-option-overlay-input\" id=\"description\" style=\"height:35px;display:inline;\"type=\"text\" name=\"Description\" placeholder=\"". $row['description'] ."\" required>
                                </fieldset>
            
                                <fieldset id=\"passwordupdate-actions\" style=\"padding-top:10px;padding-bottom:5px;\">
                                        
                                        <input type=\"submit\" id=\"passwrd-update-submit\" value=\"Update\" style=\"width:100px;height:35px;font-size:18px;padding:0px;\">
                                </fieldset>
                                
                            </div>
            
            
            
                        </form>
                    
                            
                            </div> 


                            </div>
                


                    </div> 
            
            </div>";
        
        }
    }else{

        echo "There are no images uploaded";
    }

    ?>

    </div>

    <div id="createGallery-Step3">



    </div>

</div>